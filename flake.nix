{
  description = "My website.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
        pkgs = import nixpkgs {
          inherit system;
        };

        deps = with pkgs; [
          nodejs_22 # EOL: 2027-04-30
          python312 # EOL: 2028-10-31
          python312Packages.mkdocs
          python312Packages.mkdocs-minify
          python312Packages.csscompressor
          python312Packages.htmlmin # htmlmin --keep-optional-attribute-quotes theme/favicon.svg theme/favicon.min.svg
          rubyPackages_3_3.haml # Ruby EOL: 2027-03-31
          rubyPackages_3_3.redcarpet
          inkscape
          imagemagick
        ];
      in rec
      {
        # Nix formatter, run using `$ nix fmt`
        formatter = pkgs.alejandra;

        # Exports the tildefriends package
        # Build with `$ nix build`
        packages.default = pkgs.callPackage ./default.nix {inherit deps;};

        # Creates a shell with the necessary dependencies
        # Enter using `$ nix develop`
        devShell = pkgs.mkShell {
          buildInputs = deps;
        };
      });
}
