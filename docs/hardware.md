---
unlisted: true
---

# hardware

## yaseen

Second-hand PC Specialist Recoil II

NixOS (stable, LTS kernel), Wayland + KDE6

- Intel Core i7-8750H (6C/12T) @ 4.1GHz
- GTX 1060 6GB
- 32GB DDR4/????
- 1TB SSD NVMe ???
- 15.6" FHD 144Hz anti-glare

## Misc

Mouse: G502 Hero
Keyboard: Dierya DK61(Gateron optical red switches)
Headset: JBL Tune 710BT

Phone: Google Pixel 7 (GrapheneOS)
