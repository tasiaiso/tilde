# home

> I'm open for commissions/hiring! See [here](commissions.md).

## me

Hey, I'm Tasia! `(they/them)`

Here are some random facts about me:

- I am an enby queer fox <!-- ([what the hell?](./posts/identity.md)) -->;
- I'm into computers (NixOS my beloved), self-hosting, privacy and security;
- I also like music, podcasts, reading, nature;
- I'm neurodivergent (ADHD, TBD);
- I am<!--, unfortunately,--> French (UTC+0200);
- I support FLOSS, anarcho-communism, privacy and data sovereignty;
- I should've not been allowed free speech! If I say something hurtful, feel free to let me know! I probably didn't mean it.
<!-- - I'm plural (not yet, if someone sees this, dm me "start forcing instead of looking at fedi you blithering idiot" on fedi. Thanks!) -->
<!-- - I'm a computer toucher for vulpecula.zone. -->

Here's my website. For now it's a place to put all my stuff, links and occasional writings.

```geekcode
-----BEGIN GEEK CODE BLOCK-----
Version: 3.12
GCS d- s++:+ a--- C++ UL+++>++++ ++ P L+++ E--
W++ N- o? K- w-- O? M-- V? PS+++ PE-- Y+>++
PGP+ t? 5? X? R? !tv b+ DI? D+ G e h!>-- r++ z
-----END GEEK CODE BLOCK-----
```

([?](https://web.archive.org/web/20090220181018/http://geekcode.com/geek.html)) ([decoded](https://mj.ucw.cz/geek/?code=GCS+d-+s%2b%2b%3a%2b+a---+C%2b%2b+UL%2b%2b%2b%3e%2b%2b%2b%2b+%2b%2b+P+L%2b%2b%2b+E--+W%2b%2b+N-+o%3f+K-+w--+O%3f+M--+V%3f+PS%2b%2b%2b+PE--+Y%2b%3e%2b%2b+PGP%2b+t%3f+5%3f+X%3f+R%3f+!tv+b%2b+DI%3f+D%2b+G+e+h!%3e--+r%2b%2b+z))

## how to reach me

### preferred

- Keyoxide: [F8F903DAEE2CC83176C3CF4620AB0EB62F687865](https://keyoxide.org/F8F903DAEE2CC83176C3CF4620AB0EB62F687865)
- Fediverse: [@tasiaiso@kitsunes.club](https://kitsunes.club/@tasiaiso)
- Secure Scuttlebutt: `@KdAcmKPWpiu2jXSZD8ttS0IfU2KYDJZOqy8BHumsuM4=.ed25519`
- SimpleX: [Contact address](https://simplex.chat/contact#/?v=1-4&smp=smp%3A%2F%2F1OwYGt-yqOfe2IyVHhxz3ohqo3aCCMjtB-8wn4X_aoY%3D%40smp11.simplex.im%2FZGdUasaYks8FgQdxKt5hQyNP0KLevU-O%23%2F%3Fv%3D1-2%26dh%3DMCowBQYDK2VuAyEAmnv6Lx_9akR5mNXFojrIffvqlZ6L_ANe626B6uKaNTM%253D%26srv%3D6ioorbm6i3yxmuoezrhjk6f6qgkc4syabh7m3so74xunb5nzr4pwgfqd.onion)
- E-Mail: `(my usual username)@proton.me`
- PGP: [F8F903DAEE2CC83176C3CF4620AB0EB62F687865](static/pubkey.asc)

<!-- ## alright -->

<!-- ### i might answer one day -->

<!-- - Matrix: `@tasiaiso:kitsunes.club` -->

## links

- Gitea: [tasiaiso@gitea.com](https://gitea.com/tasiaiso)
- Sourcehut: [~tasiaiso@sr.ht](https://sr.ht/~tasiaiso/)
- Gitea (Tilde Friends): [tasiaiso@dev.tildefriends.net](https://dev.tildefriends.net/tasiaiso)
- GitHub: [tasiaiso@github.com](https://github.com/tasiaiso) (absolutely proprietary)

## how to support me

If you like my posts, please consider supporting me on [Liberapay](https://liberapay.com/tasiaiso) or [Ko-fi](https://ko-fi.com/tasiaiso). Thank you!

## todo

- rss feed
- offline archives (.zim)
- gemini capsule
<!-- - more posts
    - Notifications: from Push to Pull
    - How I do productivity
    - Using `curl | bash` safely
    - A list of useful FLOSS apps
    - A post about NixOS and flakes or whatever -->

## changelog

Take a look at the whole [changelog](changelog.md)!

### v1.1.3 (2024-08-24)

- fix bug in header
- add freeplay badge
- new bio

### v1.1.2 (2024-08-07)

- new post: [NixOS: Declarative WiFi connections with agenix and NetworkManager](/~tasiaiso/docs/posts/nixos-wifi-agenix.md)

### v1.1.0, v1.1.1 (2024-08-03)

- new post: [My review of the Piscine at 42](/~tasiaiso/docs/posts/42-piscine.md)
- bugfix: change the pgp fingerprint to the new one
- citrons.xyz is back up
- unify the ad's style
- make the links' color more accessible
- add a bunch of people's badges
    - sammy
    - byte
    - elke
    - rail
    - vulpine citrus
    - soatok
    - probs other stuff too
- add cc-by-nc-sa badge
- add not by ai badge
- join no ai webring
- add [hardware](hardware.md) page
