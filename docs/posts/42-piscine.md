---
date: 2024-08-03
unlisted: false
---

# My review of the Piscine at 42

I participated in the Piscine at 42, I just finished it, and I want to go over what it was like.

42 is an alternative programming school network across the globe that relies on a peer-to-peer learning method.
The piscine ("swimming pool" in French) is the school's intensive month-long trial period during which you learn how the 42 system works, and try to adapt to it.
In practice, it is 26 days of pure work (around 10-12 hours a day are recommended), where you learn how to code, complete projects and hang out with your peers, all at the same time.

I learned a lot during that month.
I've learned how to be a somewhat efficient C developer, but also a lot of soft skills: I had to become a lot more sociable, autonomous and rigorous than I've ever been in the past.
Here I'll talk about everything we went through and experienced during this month.

In the end, I'll also talk about whether *you* should try it yourself or not.

## My experience

I'm gonna break the piscine down week by week because I think the piscine can be easily divided into 4 phases.

I want to note I'm not going to go into much details because as I see it, the less you know about the piscine, the better your experience will be.
It's like reading a book somebody spoiled the ending of two days ago.
You'll still be enjoying the book, but the element of surprise is gone.
Therefore I'll be omitting *key details* of the piscine.
Also, note that my experience is only applicable to 42 Lyon.
I can't tell you how your experience will be in another school.

First, let's talk about my background: I'm a relatively experienced TypeScript developer and daily Linux user.
I've dabbled with C a fair bit already, but nothing major.
As such, programming is already a significant part of my life.
People who have never touched a terminal in their life **will** have a much rougher piscine than I ever had.
I can't tell you how my piscine would have gone if I started from scratch, but I can tell you how mine actually did go, so let's dive in!

### Week 0

We started off with a speech from the Bocal (the school's pedagogical team) where they told us how the piscine would go, what to do if there was an issue, etc.
We waited outside for 20 minutes, then they let us in and we were off to the races.
Then I'd just log into a computer, connect to the intranet and start learning.
Some people have found the initial lack of guidance disorienting, but actually the whole shtick of the piscine is to throw you head-first in the school's way of learning (hence the name *piscine*) and see whether you could learn how to swim fast enough or not.
Of course, they wouldn't let you drown, that's where the suitably-called Lifeguards would come into action for mental health issues or whatever may be a problem for you.
Don't panic, we always have a safety net should we need it.

This is the week during which I absorbed the most knowledge.
Discovering the school as it actually is, learning how the C programming language works, living autonomously in a van near the school, etc.

Anyways, the first few exercises are making sure that you've found your bearing.
These are going to teach you how to navigate through the rest of the piscine: how to interact with the terminal and the shell, how to do simple tasks on it, etc.

Then you will be faced with the evaluation process.
You will first be evaluated by your peers, who will try to find bugs and unexpected behavior with your scripts and programs.
This allows the evaluator and evaluee to learn from each other, the objective of the school being to have students who can complement each other's skillsets to become better.

That's what sets 42 apart from the rest for me: students learn from each other.
The traditional education system establishes a wide gap between the all-knowing teacher and the student that needs to shut up and listen, and forces them to fit into a mold that most people can't fit into.
This rigid system has broken and still breaks lots of students (including myself) and their self-esteem.
42 puts students in an environment where they can and have to learn from each other, like in the real world.
This environment teaches them to cultivate their creativity, curiosity and self-confidence, instead of repressing it.

Then your work is graded by Moulinette, aka Deepthought.
It is a program that executes tests on your work in order to grade it accurately.
However, the Moulinette is a computer program and as such does not experience emotions such as compassion or care for your feelings, unlike your peers.
If you want to succeed, you'll have to become very, very, very rigorous very quickly.
If you even make an off-by-one error or forget a line break in your program output, the Moulinette will most likely notice it.

After a few days of getting to know how Linux works and how to conjure your computer into getting it to do the thing you want it to do, you'll soon start writing your own C programs.
You'll have to learn how the C language works and make simple programs with it.

> Why C ? Can't they make you learn a better and safer programming language?  
<!--  -->
> You'd be right, but you're missing a critical piece of the puzzle.  

42 itself makes no attempt at teaching you how to code, they instead provide you with tools and a framework.
You think you're learning how to code, but at 42 you actually learn how to learn.
This is, again, because the traditional education system of having one teacher yap to many students in a top-down manner is pretty bad at making you a good learner.
At 42 you learn to learn from the information superhighway (or as regular people like to call it, the internet) and your peers.
This makes you so much better at actually learning how to code later on in life.

At the end of the week, you'll be faced with the first exam.
You'll have to complete a series of exercises without access to the internet.
You'll have to rely on logic, the knowledge you gathered over the first week and the manpages.
Following this exam, you'll have your first group project (called a rush).
In group projects, you need to submit a complex program in a group.
Good collaboration techniques are essential in order to finsih the project in time, and you'll have to learn them on the fly, like everything else here.
This has been one of my most difficult experiences here, I'm absolutely not new to collaborating with other people on projects, but this added to piscine-related stress made some rushes a bit hard.

### Week 1

Now that the dust has settled a bit, I was getting used to the way 42 teaches students.
This was a good time for me to consolidate all the knowledge I had gathered during my first week.

During your piscine, you'll quickly move from writing a hello world program in C to doing rather complex mathematics and string manipulation.
The cool thing is that I didn't even notice it. One day I was walking home and I was like "Wow, 3 weeks ago I would've been totally lost doing what I'm doing right now, this is amazing !".

Throughout the piscine, a big aspect of the social experience is *coalitions*.
Think of houses in Harry Potter.
Each member has to gain points for their coalition by being logged in for a certain time each week, validating projects, sometimes through doing tasks, etc.
It's a great way to instigate healthy competition between students, as being the leading coalition would give you nothing more than bragging rights (which are extremely important here).

### Week 2

Going further into the program, this is the point where you usually start doing the real stuff: complex string manipulation and algorithms.

Throughout the piscine, active students organized events to keep us entertained: they'd cook meals for us, organize sports contests, etc.
The bocal itself would do the same: they held a lottery, where I won an enamel pin which apparently was pretty rare.
I have it on my backpack and this is one of my prized possessions from the piscine.

More about 42: the people behind this school are absolute geeks who would do absolutely anything to insert a *Hitchhiker's Guide to the Galaxy* reference here and there.
Expect to never feel the same way about the number 42 after the piscine.

This year, two 42 students created the Marvinette, a wheel of fortune we could spin every so often to get anywhere from coalition points, evaluation points or even community services if we were (un)lucky enough.
This has created a generalized gambling addiction in most piscineux.ses (me included).
It was a really cool way to blow off steam and inject dopamine into my brain at a regular interval, which was something that was sorely needed.

Over the weeks, rushes will become more and more complex.
You'll have to improvise and adapt if you wish to survive (thumbs up if you get the reference).

### Week 3

This was my last week at 42.
Things slowly started winding down. This week simultaneously went very fast and very slowly.
We spent more time fooling around this week, and to be honest, I lost some of my motivation to spend long hours coding.

The final exam is a full day spent in the cluster with lots of difficult projects to complete.
This was by far the hardest exam yet (that's why they gave us twice as much time to complete it).
Immediately after the exam, the piscine was over and I was free to go.

Leaving this place for the last time felt weird, like leaving a piece of me back there.
I spent so much time learning, working and making friends there, it felt kind of wrong.
I really hope I'll be accepted and come back here this November.

In 2 weeks after the end of the piscine, I'll receive the decision of the selection process, which I have not received yet.

### Week 4

After the piscine, things went much more slowly.
I just got used to working 10 hours a day in a fast-paced environment, having lots of social interactions and filling my brain with knowledge.
Now that everything's over though, my brain has to come to a halt and go back to my regular life.
So far it's going pretty well.

## Conclusion

My piscine was absolutely one of the most insane experiences in my life.
It taught me so much about programming, peer learning and myself too.
I wish I could do it all over again, but honestly, I'd rather be accepted the first time around :)

So now let's answer the final question "Should I go there?".

Honestly, it depends. 42 could be great for you if you think:

- you will actually like touching computers for a living (having previous experience programming is totally optional);
- you can keep up this pace for a whole month under these intense circumstances;
- you can adapt to the learning conditions I talked about previously (of course the piscine only lasts a month but the actual cursus can last around 1.5 to 4 years which are way more relaxed).

While I do believe anybody interested in computers can learn how to code, not everybody can learn efficiently with this method.
It does require a fair amount of adaptibility, rigor, motivation, sociability and availability.
Don't let yourself get discouraged though!
When I started the piscine, I only had three of those qualities.
I had to push myself out of my comfort zone to be up to the task, in the end, in my opinion, it worked out pretty well.

If you're queer, a woman or PoC, you'll feel right at home.
For those unaware (I still love you), I am non-binary, queer and neurodivergent.
42 is filled with people like me.
Finding my kin here feels absolutely great and I feel like this is one of the only places where I can keep my guard 100% down and be exactly who I am.
The team at 42 makes sure we have equal opportunities regardless of sex, gender, skin color or whatever else, which is great.

Anyway, if you think you can fit in well, feel free to look into it! It really is worth it.

> EDIT (2024-08-11): I wasn't accepted into the school.
> It's a shame, but I knew going in that not everybody will get in.
> Oh well, I'll try again next year.
>
> If you try out the piscine for yourself, good luck and let me know how it goes!
