---
date: 2024-08-22
unlisted: true
---

# curl | bash: original post

> **WARNING**: this post has been written in April of 2024.
>
> I put this post here because I had deleted it from fedi before thinking it'd be better to have it available *somewhere*.
>
> Most of the points I made here are valid, but I've realized that as a whole this is bullshit.
> In retrospect, my tone was unnecessarily heated and I did kinda miss the point.
>
> I ain't proud of this one but I do want to keep it accesible for posterity.
> My opinion has since changed; see my [blog post](./curlpipebash.md) about this. cya

For those that go crying on social media about an application telling you to `curl | bash` or even to `curl | sudo bash` because you're running arbitrary code as root:

That is useless unless you plan to carefully review and audit every line of code that runs on your computer.

Even if you do install said app, do you actually trust it's code?
Do you trust it's dependencies?
What about it's subdependencies?

There's an infinity of ways to infect an open-source repo with bad code, and some of them are actually scarily easy to perform.
Do you trust that your favorite compression utility doesn't contain code that backdoors freaking ssh (<https://nvd.nist.gov/vuln/detail/CVE-2024-3094>)?
Do you trust that a script won't remove a critical system directory because of a misplaced space (<https://github.com/MrMEEE/bumblebee-Old-and-abbandoned/issues/123>)?
Or that an ubiquitous logging library can allow remote code execution because of a bad default configuration (<https://en.m.wikipedia.org/wiki/Log4Shell>)?

I hope I can get this message stuck deep inside your head and let you know that unless you make your own operating system from scratch (including your free bootloader, kernel, gpu driver and the rest), you have to trust somebody.
And it only takes one mistake to compromise a whole distribution, or even worse.
You have to balance between having a new shiny program and having a new way to get shelled.
