---
date: 2024-06-05
unlisted: true
---

# Notifications: from Push to Pull

After reading [Amolith's post on notifications](https://secluded.site/pull-vs-push-intentional-notifications/), I thought I'd reflect on my personal experience turning off my phone's notifications and, hopefully, incite you to do the same!

## Why push notifications are bad

Notifications are distracting, and divert you from what you should really be doing.
I'm pretty sure that anybody with ADHD can relate to getting distracted by an email or a notification and spending time watching a video instead of doing the dishes.
What time is it again ? Oh right, 45 minutes just passed by. Welp.

> Exhibit A: I just got a notification from Fedi (I only get those when my client is open in the background on my phone) and that distracted me for a bit from writing this very post.

Even for people without ADHD, the human brain is notoriously bad at switching tasks efficiently.
It's a lot more efficient to allocate working time to the beginning of the day, only to wind down and watch videos on the evening.

> (do as I say, not as I do)

See this popular study: [The Cost of Interrupted Work: More Speed and Stress](https://ics.uci.edu/~gmark/chi08-mark.pdf), from the University of California, Irvine (which I did not entirely read).
It takes significant time (here, 23 minutes) to switch from one subject to another entirely.
<!-- Check -->

## What are notifiations anyway ?

There are, generally, 2 types of notifications: push notifications and pull notifications.

### Push notifications

When you think of notifications, this is most likely the first thing that comes to your mind.

They are notifications that shows up on your phone when a new Youtube video is posted, when a post is popular on Reddit or when a friend sends you a message.

<!-- Under the hood, your phone connects to a remote server and -->

> Here I'm not trying to indiscriminately bash on push notifications. There are legitimate uses for them, but most of them aren't actually that important or useful.

### Pull notifications

The second type of notifications are more intentional.
You, the user, usually have to manually open the application and pull updates from the server.
This forces you to conciously switch from one activity to another.

## Switching off

For me, because I bought a Google Pixel 7 and switched to GrapheneOS in December of 2023, I pretty much didn't have a choice; this Android distribution comes without the `Google Services Framework` by default, which is a necessary component for receiving push notifications.

Nontheless, even before I bought this new phone, I clearly realized that notifications were bad for my focus: there's always somehing interesting to watch on Youtube or Reddit, the chores can wait just a bit longer...

<!-- However, I can't spend my time getting distracted by  -->

On GrapheneOS, you can still choose to recieve notifications for certain apps (if said app supports it) at the cost of battery drain, because your phone has to make another conection to another server, instead of only pulling notifications from Google Firebase.

But this combined with the fact that it requires you to go into the settings and turn battery optimization off for that app (instead of the usual click on "Allow" when it asks you for permissions to send you notifications) means that it has to be a conscious choice, and you'll probably won't turn notifications on for more than let's say 10 apps at once.

So I flashed Graphene OS on my phone, and I started using it as my new daily driver.

> If you (understandably) don't want to buy a new phone to get rid of notifications, you can still mute specific apps' notifications and use the Do Not Disturb mode.

## The sound of silence

I quickly noticed that the lack of distractions had a noticable impact on my focus; my phone doesn't randomly go off in my pocket, or especially when I'm developing or writing.

Instead of hearing about new Youtube videos as they go online, I instead take 5 minutes each day to open NewPipe, pull down updates from the Youtube and Peertube channels I subscribe to, download the videos to watch the videos when I'm able to.
For my podcasts and RSS feeds, I use AntennaPod to get new episodes and posts each day or so, and like with NewPipe, I download them for later consumption.
I take some time every few days to read what's new on the Fediverse, Secure Scuttlebutt and my emails.

> I stil like my phone to not be in my way, so it's set on vibrate most of the time anyways

I don't get sucked into a conversation with someone online anymore. I spend time on entertainment when I want to.
Of course, that won't subtitute for good discipline when it comes to when to open said entertainment apps.

> (do as I say, ...)

It is true that this requires a bit more intention in how you use your phone (you have to get notifications instead of them coming for you themselves), but I consider this to be more of a feature than a bug.
Plus, when I *do* recieve a notification, I know it's sopmething that actually deserves my attention (a private message from a friend or family member, a weather alert, etc).
My phone's [signal to noise ratio](https://en.wikipedia.org/wiki/Signal-to-noise_ratio) is now vastly reduced.

> That even got to a point where I couldn't handle my mom's phone that's absolutely filled with what I lke to call notifications-as-a-disservice (notifications from public Telegram channels, games, useless news about celibrities she doesn't even care about, etc) that goes off every 30 seconds on average.

![A meme about your mom's phone being bright, loud, having large font sizes and lots of notifications. It features drawings from communist China propaganda](/~tasiaiso/images/posts/pushpull/phone-propangda.jpg)

## Homework

Here is today's homework:

- Turn off the notifications for all of your games and social medias!

> Is this source of notifications (Youtube channel, app, podcast, blog, etc):
>
> - A source of news related to work ?
> - A source of news related to politics or general information ?
> - A source of news related to STEM ?
> - From a friend ? (a friend's blogs) ?
>
> If so, keep it. Otherwise, disable it. You can always turn them back on as needed.

- Look into Do Not Disturb mode on your phone and see if it could make sense in your daily life.
- Try to separate personal time and work time: no personal activities on work hours, most importantly, no work activities when you're not paid to do them.
- Write about your experience on your blog or journal (If you don't have one yet, [make one already!](TODO)). Write about it each 3 to 7 days for a month or two.
- Contact me once you're done and talk to me about your experience, or send me your journal. I might put your feedback on this page for others to read.
