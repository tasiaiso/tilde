---
unlisted: true
---

# Commissions

I'm available for commissions!

## About me

I'm a French computer science student looking for work in IT.

I have extensive knowledge in TypeScript and JavaScript since 2018, especially in the Node.js ecosystem.
I have been using GNU+Linux operating systems since 2019, with good experience in the following distributions:

- Ubuntu-based distros and Ubuntu Server,
- Arch Linux;
- NixOS.

I'm proficient with the Nix package manager and with Docker.
Additionally, I've been researching supply chain security for a while.
I can probably help make your software a bit more secure!

Also, I know a fair bit about SQL databases, MongoDB, Bash, Mkdocs, Express, Lit and Electron.

I put an emphasis on clean code, using standard software developement principles (DRY, KISS, etc).
I enjoy writing minimalist, efficient and secure code.
I'm dynamic, autonomous and willing to learn any technology in your stack!

I have experience making and maintaining:

- Discord bots;
- Telegram bots;
- Websites;

But I can work on pretty much anything!

I speak French (N) and English (C1).

If you need somebody to work on your project, or if you think I could fit in your team, feel free to reach out to me at `(my username)@proton.me` - Thanks!
