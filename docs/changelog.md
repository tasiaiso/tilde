---
unlisted: true
---

# changelog

<!-- Before pushing a new version:
    - Remember to copy the changelog in index.md and delete the oldest entry
    - Change 2024-xx-xx
    - change version in package.json
    - $ npm i
    - $ make upload
-->
<!-- - put advertisement behind a `details` block -->

## v1.1.3 (2024-08-24)

- fix bug in header
- add freeplay badge
- new bio

## v1.1.2 (2024-08-07)

- new post: [NixOS: Declarative WiFi connections with agenix and NetworkManager](/~tasiaiso/docs/posts/nixos-wifi-agenix.md)

## v1.1.0, v1.1.1 (2024-08-03)

- new post: [My review of the Piscine at 42](/~tasiaiso/docs/posts/42-piscine.md)
- bugfix: change the pgp fingerprint to the new one
- citrons.xyz is back up
- unify the ad's style
- make the links' color more accessible
- add a bunch of people's badges
    - sammy
    - byte
    - elke
    - rail
    - vulpine citrus
    - soatok
    - probs other stuff too
- add cc-by-nc-sa badge
- add not by ai badge
- join no ai webring
- add [hardware](hardware.md) page

## v1.0.3 (2024-06-05)

- new commissions page
- add metamuffin ads
- major optimizations (artifact compression, html lazy loading)
- got markdown working in haml
- add a content security policy (to check)
- fix an indentation bug in the haml template that caused the footer to show up after `</body></html>`
- add maia badge
- add warrant canaray
- use my keyoxide pgp key instead
- add "last updated" field at the bottom of the page
- citrons.xyz is unresponsive, hide johnvertisement

## v1.0.2 (2024-05-26)

- add tea badge
- add linux badge
- add xeiaso badge
- move build scripts to a Makefile
- add johnvertisement
- use haml for the main template
- speed up page load by lazy loading badges and johnvertisement

## v1.0.1 (2024-05-18)

- add links to my git accounts, ko-fi and liberapay
- add link to the apioform memetic page
- this is now an actual website
- fix wording
- add legal stuff at the bottom of the page
- now for hire

## v1.0.0 (2024-05-09)

- fixed url paths bugs
- gruvbox theme
- modified page layout
- new meta fields (wip)
- added webring tilde.club
- added badge fediring, tildeclub & kitsulife
- updated bio
- updated build process
- posts now use yaml front matter
- posts date displayed in the nav menu
- updated keyoxide id
- added this changelog section (no way)
