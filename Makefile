# $< : first dependency (input)
# $@ : rule name (output)

# Build the website
site/index.html: docs/** \
				theme/** \
				theme/main.html \
				theme/style.min.css \
				theme/gruvbox-dark.min.css \
				mkdocs.yml \
				favicon_all
	mkdocs build

build: site/index.html

# HTML
theme/main.html: haml/main.haml
	haml render $< > $@.tmp
	cat $@.tmp > $@
	rm $@.tmp

# CSS
theme/style.min.css: theme/style.css
	python3 -m csscompressor -o $@ $<

theme/gruvbox-dark.min.css: theme/gruvbox-dark.css
	python3 -m csscompressor -o $@ $<

# Favicons
theme/favicon-ex.png: theme/favicon.svg
	inkscape theme/favicon.svg --export-filename=theme/favicon-ex.png

theme/favicon.png: theme/favicon-ex.png
	magick theme/favicon-ex.png -quality 95 -resize 144x144 theme/favicon.png

# theme/favicon.ico: theme/favicon-ex.png
# 	magick theme/favicon-ex.png -compress Zip -quality 95 -resize 144x144 -background transparent theme/favicon.ico

favicon_all: theme/favicon.png
# theme/favicon.ico

# Scripts

# Serve the website at http://127.0.0.1:8000/~tasiaiso/
serve: site/index.html
	mkdocs serve -w theme

format: format-code format-markdown

format-code:
	npx prettier --cache --write --check .

format-markdown:
	npx markdownlint-cli 'docs/**/*.md' -f

# Upload the website to tilde.club
upload: clean build
	scp -r site/** tasiaiso@tilde.club:/home/tasiaiso/public_html

purge: clean format build

.PHONY: clean
clean:
	rm -r site/ || true
	rm theme/main.html || true
	rm theme/style.min.css || true
	rm theme/gruvbox-dark.min.css || true
	rm theme/favicon-ex.png || true
	rm theme/favicon.min.svg || true
	rm theme/favicon.png || true
	rm theme/favicon.ico || true
	