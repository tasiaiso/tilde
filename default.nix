{
  pkgs,
  lib,
  deps,
}:
pkgs.stdenv.mkDerivation rec {
  pname = "tilde";
  version = "1.1.3";

  src = pkgs.fetchFromGitea {
    domain = "gitea.com";
    owner = "tasiaiso";
    repo = "tilde";
    rev = "4f8edb0520f32a6c5cbf84229e3a342859748021";
    hash = "sha256-9vNzhIrWwyCcq+VR5FUJl44A1WG8x/QIWN6BkugWfdc=";
  };

  nativeBuildInputs = deps;

  buildPhase = ''
    make
  '';

  installPhase = ''
    mkdir -p $out/
    cp -r site/. $out/
  '';
}
